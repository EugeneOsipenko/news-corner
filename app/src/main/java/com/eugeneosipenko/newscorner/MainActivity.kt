package com.eugeneosipenko.newscorner

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eugeneosipenko.newscorner.atricles.ArticlesActivity
import com.eugeneosipenko.newscorner.sources.SourcesAdapter
import com.eugeneosipenko.newscorner.sources.SourcesViewModel
import kotlinx.android.synthetic.main.activity_list.*

class MainActivity : AppCompatActivity() {

    private val model: SourcesViewModel by lazy { ViewModelProviders.of(this)[SourcesViewModel::class.java] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        supportActionBar?.setSubtitle(R.string.powered_by_news_api)

        val adapter = SourcesAdapter { source ->
            startActivity(Intent(this, ArticlesActivity::class.java).apply {
                putExtra(Constants.KEY_SOURCE_ID, source.id)
            })
        }

        list.adapter = adapter
        model.getSourcesLiveData().observe(this, Observer { sources ->
            sources?.let {
                adapter.setItems(it.sources)
                adapter.notifyDataSetChanged()
            }
        })
    }
}
