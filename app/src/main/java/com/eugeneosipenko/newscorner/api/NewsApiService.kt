package com.eugeneosipenko.newscorner.api

import com.eugeneosipenko.newscorner.BuildConfig
import com.eugeneosipenko.newscorner.model.Sources
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApiService {

    @GET("v2/sources")
    fun getSources(): Single<Sources>

    @GET("v2/top-headlines")
    fun getArticles(
            @Query("page") page: Int? = null,
            @Query("sources") sources: String? = null
    ): Single<ArticlesResponse>

    companion object {

        val API: NewsApiService by lazy { create() }

        private fun create(): NewsApiService {
            val client = OkHttpClient.Builder()
                    .addInterceptor { chain: Interceptor.Chain ->
                        val request = chain
                                .request()
                                .newBuilder()
                                .addHeader("X-Api-Key", BuildConfig.API_KEY)
                                .build()

                        chain.proceed(request)
                    }
                    .build()

            return Retrofit.Builder()
                    .baseUrl("https://newsapi.org/")
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(NewsApiService::class.java)
        }
    }
}