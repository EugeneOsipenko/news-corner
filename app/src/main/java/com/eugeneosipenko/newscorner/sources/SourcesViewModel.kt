package com.eugeneosipenko.newscorner.sources

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.ViewModel
import com.eugeneosipenko.newscorner.api.NewsApiService
import com.eugeneosipenko.newscorner.model.Sources
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SourcesViewModel : ViewModel() {

    private val sourcesLiveData = LiveDataReactiveStreams.fromPublisher(
            NewsApiService.API.getSources()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable())

    fun getSourcesLiveData(): LiveData<Sources> = sourcesLiveData
}