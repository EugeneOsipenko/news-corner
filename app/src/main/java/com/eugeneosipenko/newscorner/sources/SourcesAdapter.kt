package com.eugeneosipenko.newscorner.sources

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eugeneosipenko.newscorner.R
import com.eugeneosipenko.newscorner.model.Source
import kotlinx.android.synthetic.main.item_source.view.*

class SourcesAdapter(
        val itemClickHandler: (Source) -> Unit
) : RecyclerView.Adapter<SourcesAdapter.SourceViewHolder>() {

    private val items = mutableListOf<Source>()

    fun setItems(newItems: List<Source>) {
        items.clear()
        items.addAll(newItems)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SourceViewHolder {
        return SourceViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_source, parent, false))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SourceViewHolder, position: Int) {
        val source = items[position]
        holder.source = source
        holder.view.title.text = source.name
        holder.view.url.text = source.url
        holder.view.description.text = source.description
    }

    inner class SourceViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        var source: Source? = null

        init {
            view.setOnClickListener { source?.let { itemClickHandler.invoke(it) } }
        }
    }
}