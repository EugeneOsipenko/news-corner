package com.eugeneosipenko.newscorner.atricles

import android.arch.paging.PageKeyedDataSource
import com.eugeneosipenko.newscorner.api.ArticlesResponse
import com.eugeneosipenko.newscorner.api.NewsApiService
import com.eugeneosipenko.newscorner.model.Article
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class ArticleDataSource(
        private val sourceId: String
) : PageKeyedDataSource<Int, Article>() {

    private val api = NewsApiService.API

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Article>) {
        api.getArticles(sources = sourceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response: ArticlesResponse? ->
                    response?.let { callback.onResult(response.articles, 0, response.totalResults, null, 1) }
                }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        api.getArticles(params.key, sourceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response: ArticlesResponse? ->
                    response?.let { callback.onResult(response.articles, params.key + 1) }
                }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        //no-op
    }
}