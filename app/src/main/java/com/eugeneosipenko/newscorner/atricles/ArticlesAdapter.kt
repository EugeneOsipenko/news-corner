package com.eugeneosipenko.newscorner.atricles

import android.arch.paging.PagedListAdapter
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.eugeneosipenko.newscorner.GlideApp
import com.eugeneosipenko.newscorner.R
import com.eugeneosipenko.newscorner.model.Article
import kotlinx.android.synthetic.main.item_article.view.*
import java.text.SimpleDateFormat
import java.util.*

class ArticlesAdapter(
        val itemClickHandler: (Article) -> Unit
) : PagedListAdapter<Article, ArticlesAdapter.ArticleViewHolder>(ArticleDiffUtilCallback()) {

    companion object {
        private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false))
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        holder.itemView.apply {
            val article = getItem(position)!!

            holder.article = article

            title.text = article.title
            description.text = article.description
            author.text = article.author
            publishedAt.text = getRelativeDate(article.publishedAt)

            GlideApp.with(this)
                    .load(article.urlToImage)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(image)
        }
    }

    private fun getRelativeDate(date: String): CharSequence {
        return DateUtils.getRelativeTimeSpanString(dateFormat.parse(date.substring(0, 18)).time)
    }

    inner class ArticleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var article: Article? = null

        init {
            view.setOnClickListener { article?.let { itemClickHandler.invoke(it) } }
        }
    }
}