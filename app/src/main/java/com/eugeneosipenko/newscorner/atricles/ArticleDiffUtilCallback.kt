package com.eugeneosipenko.newscorner.atricles

import android.support.v7.util.DiffUtil
import android.text.TextUtils
import com.eugeneosipenko.newscorner.model.Article

class ArticleDiffUtilCallback : DiffUtil.ItemCallback<Article>() {

    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean {
        return TextUtils.equals(oldItem.url, newItem.url)
    }

    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean {
        return TextUtils.equals(newItem.author, oldItem.author)
                && TextUtils.equals(newItem.content, oldItem.content)
                && TextUtils.equals(newItem.description, oldItem.description)
                && TextUtils.equals(newItem.publishedAt, oldItem.publishedAt)
                && TextUtils.equals(newItem.title, oldItem.title)
                && TextUtils.equals(newItem.urlToImage, oldItem.urlToImage)
    }
}