package com.eugeneosipenko.newscorner.atricles

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider

class ArticleViewModelFactory(
        private val sourceId: String
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ArticleViewModel(sourceId) as T
    }
}