package com.eugeneosipenko.newscorner.atricles

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eugeneosipenko.newscorner.Constants
import com.eugeneosipenko.newscorner.R
import kotlinx.android.synthetic.main.activity_list.*

class ArticlesActivity : AppCompatActivity() {

    private val viewModel: ArticleViewModel by lazy {
        ViewModelProviders.of(
                this,
                ArticleViewModelFactory(intent.getStringExtra(Constants.KEY_SOURCE_ID)))
                .get(ArticleViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val adapter = ArticlesAdapter { article -> startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(article.url))) }

        viewModel.articleLiveData.observe(this, Observer(adapter::submitList))
        list.adapter = adapter
    }
}