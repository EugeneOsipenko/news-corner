package com.eugeneosipenko.newscorner.atricles

import android.arch.paging.DataSource
import com.eugeneosipenko.newscorner.model.Article


class ArticleDataFactory(
        private val sourceId: String
) : DataSource.Factory<Int, Article>() {

    override fun create(): DataSource<Int, Article> {
        return ArticleDataSource(sourceId)
    }
}