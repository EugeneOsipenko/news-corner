package com.eugeneosipenko.newscorner.atricles

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.eugeneosipenko.newscorner.model.Article
import java.util.concurrent.Executor
import java.util.concurrent.Executors




class ArticleViewModel(
        sourceId: String
) : ViewModel() {

    val executor: Executor
    val articleLiveData: LiveData<PagedList<Article>>

    init {
        executor = Executors.newSingleThreadExecutor()

        val pagedListConfig = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(10)
                .setPageSize(20)
                .build()

        articleLiveData = LivePagedListBuilder(ArticleDataFactory(sourceId), pagedListConfig)
                .setFetchExecutor(executor)
                .build()
    }
}